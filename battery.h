#include "Arduino.h"

class lipo {
  public:
    void setSerial(HardwareSerial *SerialCom);
    boolean isBatteryOkay();

    
  private:
    HardwareSerial *SerialCom;

  
};

