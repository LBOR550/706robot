#include "battery.h"


void lipo::setSerial(HardwareSerial *SerialCom) {
  this->SerialCom = SerialCom;
}

boolean lipo::isBatteryOkay() {
  static byte Low_voltage_counter;
  static unsigned long previous_millis;

  int Lipo_level_cal;
  int raw_lipo;

  raw_lipo = analogRead(A0);
  Lipo_level_cal = (raw_lipo - 717);
  Lipo_level_cal = Lipo_level_cal * 100;
  Lipo_level_cal = Lipo_level_cal / 143;

  if (Lipo_level_cal > 0 && Lipo_level_cal < 160) {
    previous_millis = millis();
    this->SerialCom->print("Lipo level:");
    this->SerialCom->print(Lipo_level_cal);
    this->SerialCom->print("%");
    this->SerialCom->print(" : Raw Lipo:");
    this->SerialCom->println(raw_lipo);
    Low_voltage_counter = 0;
    return true;
  } else {
    Low_voltage_counter++;
    if (Low_voltage_counter > 5)
      return false;
    else
      return true;
  }

  
  
   
}

