#include <Servo.h>
#include "locomotion.h"
#include <FaBo9Axis_MPU9250.h>
#include "PID.h"
//#include "battery.h"

FaBo9Axis fabo;

enum STATE {
  INITIALISING,
  RUNNING,
  STOPPED
};

const byte left_front = 46;
const byte left_rear = 47;
const byte right_rear = 50;
const byte right_front = 51;

  float xn, yn, zn;
float prevX; float X = 0;
float prevY; float Y = 0;
float prevZ; float Z = 0;

float moveAvg[5] = {0, 0 ,0, 0, 0};

boolean inMotion = false;

HardwareSerial *SerialCom;
locomotion motorSystem;

PID *pidX, *pidY, *pidZ;

unsigned long currentTime, prevTime;

void setup() {

  
  SerialCom = &Serial1;
  
  pidX = new PID();
  pidY = new PID();
  pidZ = new PID();
  
  SerialCom->begin(115200);
  SerialCom->println("Starting......");
  delay(1000);



}

void loop() {
  static STATE machine_state = INITIALISING;
  //FSM

  switch (machine_state) {
     case INITIALISING:
          machine_state=initialise();
          break;
     case RUNNING:
           machine_state=runCycle();
          break;
     case STOPPED:
          machine_state=stopping();
          break;
    
  };
}


STATE initialise() {
  SerialCom->println("START");
  SerialCom->println("Setting up locomotion");
  motorSystem.setupLocomotion(left_front, left_rear, right_rear, right_front);
  SerialCom->println("MPU");
  fabo.begin();
  pidX->setGains(1, 0, 0);
  pidY->setGains(1, 0, 0);
  pidZ->setGains(1, 0, 0);
  return RUNNING;
  
}


STATE runCycle() {
  static unsigned long previous_millis;
  
  read_serial_command();

  
  if (millis() - previous_millis > 500) {
    previous_millis = millis();

    runMotors();
    if (is_battery_voltage_OK() == false) {
      return STOPPED;
    }
  }
  return RUNNING;
  
}


///BATTERY SAFETY SECTION
STATE stopping() {
  static byte counter_lipo_voltage_ok;
  static unsigned long previous_millis;

  motorSystem.disable_motors();
//  slow_flash_LED_builtin();

 
  if (millis() - previous_millis > 500) { //print massage every 500ms
    previous_millis = millis();
    SerialCom->println("Lipo voltage too LOW, any lower and the lipo with be damaged");
    SerialCom->println("Please Re-charge Lipo");

    //500ms timed if statement to check lipo and output speed settings
    if (is_battery_voltage_OK()) {
      SerialCom->print("Lipo OK Counter:");
      SerialCom->println(counter_lipo_voltage_ok);
      counter_lipo_voltage_ok++;
      if (counter_lipo_voltage_ok > 10) { //Making sure lipo voltage is stable
        counter_lipo_voltage_ok = 0;
        motorSystem.enable_motors();
        SerialCom->println("Lipo OK returning to RUN STATE");
        return RUNNING;
      }
    } else counter_lipo_voltage_ok = 0;
  }
  return STOPPED;
}


void runMotors() {

  currentTime = millis();
  float ax, ay, az, gx, gy, gz;

  float outX, outY, outZ;

  
  
  fabo.readAccelXYZ(&ax, &ay, &az);
  fabo.readGyroXYZ(&gx, &gy, &gz);

  xn = ax *9.81 * (currentTime - prevTime) + prevX;
  yn = ay *9.81 * (currentTime - prevTime) + prevY;
  zn = gz*2*3.14/360;
  
  SerialCom->println("Raw Sensor");
  SerialCom->print("X: ");
  SerialCom->print(ax);
  SerialCom->println(" ");
  
  SerialCom->print("Y: ");
  SerialCom->print(ay);
  SerialCom->println(" ");
  
  SerialCom->print("Z: ");
  SerialCom->print(gz);
  SerialCom->println(" ");

  SerialCom->println("Detected Speed");
  SerialCom->print("X: ");
  SerialCom->print(xn);
  SerialCom->println(" ");
  
  SerialCom->print("Y: ");
  SerialCom->print(yn);
  SerialCom->println(" ");
  
  SerialCom->print("Z: ");
  SerialCom->print(zn);
  SerialCom->println(" ");
  
  
  outX = pidX->applyController((X - xn));
  outY = pidY->applyController((Y - yn));
  outZ = pidZ->applyController((Z - zn));

  
/*
  if (inMotion  ==true ){
    motorSystem.IK(outX,outY,outZ, SerialCom);
  } else {
    motorSystem.STOP();
  }*/
 


  prevX = xn;
  prevY = yn;
  prevZ = zn;
  
  prevTime = currentTime;
  
  
}


boolean is_battery_voltage_OK()
{
  static byte Low_voltage_counter;
  static unsigned long previous_millis;

  int Lipo_level_cal;
  int raw_lipo;
  //the voltage of a LiPo cell depends on its chemistry and varies from about 3.5V (discharged) = 717(3.5V Min) https://oscarliang.com/lipo-battery-guide/
  //to about 4.20-4.25V (fully charged) = 860(4.2V Max)
  //Lipo Cell voltage should never go below 3V, So 3.5V is a safety factor.
  raw_lipo = analogRead(A0);
  Lipo_level_cal = (raw_lipo - 717);
  Lipo_level_cal = Lipo_level_cal * 100;
  Lipo_level_cal = Lipo_level_cal / 143;

  if (Lipo_level_cal > 0 && Lipo_level_cal < 160) {
    previous_millis = millis();
    SerialCom->print("Lipo level:");
    SerialCom->print(Lipo_level_cal);
    SerialCom->print("%");
    SerialCom->print(" : Raw Lipo:");
    SerialCom->println(raw_lipo);
    Low_voltage_counter = 0;
    return true;
  } else {
    Low_voltage_counter++;
    if (Low_voltage_counter > 5)
      return false;
    else
      return true;
  }

}


void read_serial_command()
{
  if (SerialCom->available()) {
    char val = SerialCom->read();
    SerialCom->print("Speed:");
    SerialCom->print(" ms ");

    //Perform an action depending on the command
    switch (val) {
      case 'w'://Move Forward
      case 'W':
        prevX = 0;
        prevY = 0;
        prevZ = 0;
        xn = 0;
        yn = 0;
        zn = 0;
        motorSystem.IK(0,10,0, SerialCom);
        SerialCom->println("Forward");
        break;
      case 's'://Move Backwards
      case 'S':
        prevX = 0;
        prevY = 0;
        prevZ = 0;
        xn = 0;
        yn = 0;
        zn = 0;
        motorSystem.IK(0,-10,0 , SerialCom);
        SerialCom->println("Backwards");
        break;
      case 'q'://Turn Left
      case 'Q':        
        prevX = 0;
        prevY = 0;
        prevZ = 0;        
        xn = 0;
        yn = 0;
        zn = 0;
        motorSystem.IK(10,0,0, SerialCom);
        SerialCom->println("Strafe Left");
        break;
      case 'e'://Turn Right
      case 'E':
        prevX = 0;
        prevY = 0;
        prevZ = 0;
        xn = 0;
        yn = 0;
        zn = 0;
        motorSystem.IK(-10,0,0, SerialCom);
        SerialCom->println("Strafe Right");
        break;
      case 'a'://Turn Right
      case 'A':
        prevX = 0;
        prevY = 0;
        prevZ = 0;
        xn = 0;
        yn = 0;
        zn = 0;
        motorSystem.IK(0,0,50, SerialCom);
        SerialCom->println("ccw");
        break;
      case 'd'://Turn Right
      case 'D':
        prevX = 0;
        prevY = 0;
        prevZ = 0;
        xn = 0;
        yn = 0;
        zn = 0;
        motorSystem.IK(0,0,-50, SerialCom);
        SerialCom->println("cw");
        break;
      case '-'://Turn Right

      default:
        prevX = 0;
        prevY = 0;
        prevZ = 0;
        xn = 0;
        yn = 0;
        zn = 0;
        motorSystem.STOP();
        SerialCom->println("stop");
        break;
    }
  }
}

void read_serial_command2()
{
  if (SerialCom->available()) {
    char val = SerialCom->read();
    SerialCom->print("Speed:");
    SerialCom->print(" ms ");

    //Perform an action depending on the command
    switch (val) {
      case 'w'://Move Forward
      case 'W':
        prevX = 0;
        prevY = 0;
        prevZ = 0;
      
        X = 0;
        Y = 10;
        Z = 0;
        inMotion = true;
        
        SerialCom->println("Forward");
        break;
      case 's'://Move Backwards
      case 'S':
        prevX = 0;
        prevY = 0;
        prevZ = 0;
      
        X = 0;
        Y = -10;
        Z = 0;
        inMotion = true;
        SerialCom->println("Backwards");
        break;
      case 'q'://strafeLeft
      case 'Q':
        prevX = 0;
        prevY = 0;
        prevZ = 0;
      
        X = 10;
        Y = 0;
        Z = 0;
        inMotion = true;
        SerialCom->println("Strafe Left");
        break;
      case 'e'://strafeRight
      case 'E':
        prevX = 0;
        prevY = 0;
        prevZ = 0;
      
        X = -10;
        Y = 0;
        Z = 0;
        inMotion = true;
        SerialCom->println("Strafe Right");
        break;
      case 'a'://Turn Right
      case 'A':
        prevX = 0;
        prevY = 0;
        prevZ = 0;

      
        X = 0;
        Y = 0;
        Z = 50;
        inMotion = true;
        SerialCom->println("ccw");
        break;
      case 'd'://Turn Right
      case 'D':
        prevX = 0;
        prevY = 0;
        prevZ = 0;
      
        X = 0;
        Y = 0;
        Z = -50;
        inMotion = true;
        SerialCom->println("cw");
        break;
      case '-'://Turn Right

      default:
        prevX = 0;
        prevY = 0;
        prevZ = 0;
        X = 0;
        Y = 0;
        Z = 0;
        inMotion = false;
        SerialCom->println("stop");
        break;
    }
  }
}



